package br.com.api.condomanager.condomanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.api.condomanager.condomanager.model.OcorrenciaEntity;

public interface OcorrenciaRepository extends JpaRepository<OcorrenciaEntity, Long> {
	
}
