package br.com.api.condomanager.condomanager.sistema.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetResponseDTO {
	
	private String nome;
	private String tipo;
	
}
